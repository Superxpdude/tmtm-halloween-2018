/*
	SXP_fnc_zSpawnArea
	Author: Superxpdude
	Spawns a number of zombies within an area
	
	Executes only on the server
	
	Parameters:
		0: Trigger - Trigger that defines the spawn area
		1: Scalar - Number of zombies to spawn
		2: Array - Weights of different zombies to spawn
			OR
		2: String - Preset selection of weights to use
		3: Scalar - Spawn type. 0 for civs only, 1 for soldiers only, 2 for a mix
	
	Returns: Nothing
*/
#define DEFAULTZOMBIEAMOUNT 20
#define DEFAULTMAXGROUPSIZE 5

// Only run on the server
if (!isServer) exitWith {};

// Define our parameters
params [
	["_trigger", nil, [objNull]],
	["_amount", 0, [0]],
	["_weights", [1,1,0.3,0], [[],""], 4],
	["_spawnType", 0, [0]]
];

(triggerArea _trigger) params [
	["_triggerX", 0],
	["_triggerY", 0],
	["_triggerAngle", 0],
	["_triggerRectangle", false]
];

private ["_array", "_civArray", "_soldierArray", "_allArray"];

_fastCiv = ["RyanZombieC_man_1", "RyanZombieC_man_polo_1_F", "RyanZombieC_man_polo_2_F", "RyanZombieC_man_polo_4_F", "RyanZombieC_man_polo_5_F", "RyanZombieC_man_polo_6_F", "RyanZombieC_man_p_fugitive_F", "RyanZombieC_man_w_worker_F", "RyanZombieC_scientist_F", "RyanZombieC_man_hunter_1_F", "RyanZombieC_man_pilot_F", "RyanZombieC_journalist_F", "RyanZombieC_Orestes", "RyanZombieC_Nikos", "RyanZombie15", "RyanZombie16", "RyanZombie17", "RyanZombie18", "RyanZombie19", "RyanZombie20", "RyanZombie21", "RyanZombie22", "RyanZombie23", "RyanZombie24", "RyanZombie25", "RyanZombie26", "RyanZombie27", "RyanZombie28", "RyanZombie29", "RyanZombie30", "RyanZombie31", "RyanZombie32"];
_mediumCiv = ["RyanZombieC_man_1medium", "RyanZombieC_man_polo_1_Fmedium", "RyanZombieC_man_polo_2_Fmedium", "RyanZombieC_man_polo_4_Fmedium", "RyanZombieC_man_polo_5_Fmedium", "RyanZombieC_man_polo_6_Fmedium", "RyanZombieC_man_p_fugitive_Fmedium", "RyanZombieC_man_w_worker_Fmedium", "RyanZombieC_scientist_Fmedium", "RyanZombieC_man_hunter_1_Fmedium", "RyanZombieC_man_pilot_Fmedium", "RyanZombieC_journalist_Fmedium", "RyanZombieC_Orestesmedium", "RyanZombieC_Nikosmedium", "RyanZombie15medium", "RyanZombie16medium", "RyanZombie17medium", "RyanZombie18medium", "RyanZombie19medium", "RyanZombie20medium", "RyanZombie21medium", "RyanZombie22medium", "RyanZombie23medium", "RyanZombie24medium", "RyanZombie25medium", "RyanZombie26medium", "RyanZombie27medium", "RyanZombie28medium", "RyanZombie29medium", "RyanZombie30medium", "RyanZombie31medium", "RyanZombie32medium"];
_slowCiv = ["RyanZombieC_man_1slow", "RyanZombieC_man_polo_1_Fslow", "RyanZombieC_man_polo_2_Fslow", "RyanZombieC_man_polo_4_Fslow", "RyanZombieC_man_polo_5_Fslow", "RyanZombieC_man_polo_6_Fslow", "RyanZombieC_man_p_fugitive_Fslow", "RyanZombieC_man_w_worker_Fslow", "RyanZombieC_scientist_Fslow", "RyanZombieC_man_hunter_1_Fslow", "RyanZombieC_man_pilot_Fslow", "RyanZombieC_journalist_Fslow", "RyanZombieC_Orestesslow", "RyanZombieC_Nikosslow", "RyanZombie15slow", "RyanZombie16slow", "RyanZombie17slow", "RyanZombie18slow", "RyanZombie19slow", "RyanZombie20slow", "RyanZombie21slow", "RyanZombie22slow", "RyanZombie23slow", "RyanZombie24slow", "RyanZombie25slow", "RyanZombie26slow", "RyanZombie27slow", "RyanZombie28slow", "RyanZombie29slow", "RyanZombie30slow", "RyanZombie31slow", "RyanZombie32slow"];
_walkerCiv = ["RyanZombieC_man_1Walker", "RyanZombieC_man_polo_1_FWalker", "RyanZombieC_man_polo_2_FWalker", "RyanZombieC_man_polo_4_FWalker", "RyanZombieC_man_polo_5_FWalker", "RyanZombieC_man_polo_6_FWalker", "RyanZombieC_man_p_fugitive_FWalker", "RyanZombieC_man_w_worker_FWalker", "RyanZombieC_scientist_FWalker", "RyanZombieC_man_hunter_1_FWalker", "RyanZombieC_man_pilot_FWalker", "RyanZombieC_journalist_FWalker", "RyanZombieC_OrestesWalker", "RyanZombieC_NikosWalker", "RyanZombie15walker", "RyanZombie16walker", "RyanZombie17walker", "RyanZombie18walker", "RyanZombie19walker", "RyanZombie20walker", "RyanZombie21walker", "RyanZombie22walker", "RyanZombie23walker", "RyanZombie24walker", "RyanZombie25walker", "RyanZombie26walker", "RyanZombie27walker", "RyanZombie28walker", "RyanZombie29walker", "RyanZombie30walker", "RyanZombie31walker", "RyanZombie32walker"];

_fastSoldier = ["RyanZombieB_Soldier_02_f", "RyanZombieB_Soldier_02_f_1", "RyanZombieB_Soldier_02_f_1_1", "RyanZombieB_Soldier_03_f", "RyanZombieB_Soldier_03_f_1", "RyanZombieB_Soldier_03_f_1_1", "RyanZombieB_Soldier_04_f", "RyanZombieB_Soldier_04_f_1", "RyanZombieB_Soldier_04_f_1_1", "RyanZombieB_Soldier_lite_F", "RyanZombieB_Soldier_lite_F_1"];
_mediumSoldier = ["RyanZombieB_Soldier_02_fmedium", "RyanZombieB_Soldier_02_f_1medium", "RyanZombieB_Soldier_02_f_1_1medium", "RyanZombieB_Soldier_03_fmedium", "RyanZombieB_Soldier_03_f_1medium", "RyanZombieB_Soldier_03_f_1_1medium", "RyanZombieB_Soldier_04_fmedium", "RyanZombieB_Soldier_04_f_1medium", "RyanZombieB_Soldier_04_f_1_1medium", "RyanZombieB_Soldier_lite_Fmedium", "RyanZombieB_Soldier_lite_F_1medium"];
_slowSoldier = ["RyanZombieB_Soldier_02_fslow", "RyanZombieB_Soldier_02_f_1slow", "RyanZombieB_Soldier_02_f_1_1slow", "RyanZombieB_Soldier_03_fslow", "RyanZombieB_Soldier_03_f_1slow", "RyanZombieB_Soldier_03_f_1_1slow", "RyanZombieB_Soldier_04_fslow", "RyanZombieB_Soldier_04_f_1slow", "RyanZombieB_Soldier_04_f_1_1slow", "RyanZombieB_Soldier_lite_Fslow", "RyanZombieB_Soldier_lite_F_1slow"];
_walkerSoldier = ["RyanZombieB_Soldier_02_fWalker", "RyanZombieB_Soldier_02_f_1Walker", "RyanZombieB_Soldier_02_f_1_1Walker", "RyanZombieB_Soldier_03_fWalker", "RyanZombieB_Soldier_03_f_1Walker", "RyanZombieB_Soldier_03_f_1_1Walker", "RyanZombieB_Soldier_04_fWalker", "RyanZombieB_Soldier_04_f_1Walker", "RyanZombieB_Soldier_04_f_1_1Walker", "RyanZombieB_Soldier_lite_FWalker", "RyanZombieB_Soldier_lite_F_1Walker"];

_demons = ["RyanZombieboss1", "RyanZombieboss2", "RyanZombieboss3", "RyanZombieboss4", "RyanZombieboss5", "RyanZombieboss6", "RyanZombieboss7", "RyanZombieboss8", "RyanZombieboss9", "RyanZombieboss10", "RyanZombieboss11", "RyanZombieboss12", "RyanZombieboss13", "RyanZombieboss14", "RyanZombieboss15", "RyanZombieboss16", "RyanZombieboss17", "RyanZombieboss18", "RyanZombieboss19", "RyanZombieboss20", "RyanZombieboss21", "RyanZombieboss22", "RyanZombieboss23", "RyanZombieboss24", "RyanZombieboss25", "RyanZombieboss26", "RyanZombieboss27", "RyanZombieboss28", "RyanZombieboss29", "RyanZombieboss30", "RyanZombieboss31", "RyanZombieboss32"];

_civArray = [_walkerCiv, _slowCiv, _mediumCiv, _fastCiv];
_soldierArray = [_walkerSoldier, _slowSoldier, _mediumSoldier, _fastSoldier];
_allArray = [_walkerCiv + _walkerSoldier, _slowCiv + _slowSoldier, _mediumCiv + _mediumSoldier, _fastCiv + _fastSoldier];
_defaultWeight = [1, 1, 0.7, 0.3];

_array = switch (_spawnType) do {
	case 0: {_civArray;};
	case 1: {_soldierArray;};
	case 2: {_allArray;};
};

// If the "weights" param was a string, match it to a standard weight set
if ((typeName _weights) == "STRING") then {
	switch (_weights) do {
		case "spawnarea": {_weights = [1,1,0,0];}; // No fast zombies near spawn, mostly slow
		case "town_easy": {_weights = [1,0.7,0,0];}; // Slow zombies, but large quantities
		case "town_med": {_weights = [0.8,1,0.2,0];}; // Shifts slightly towards faster zombies
		case "town_hard": {_weights = [0.4,1,0.6,0];}; // More of a shift towards medium zombies. No fast zombies for towns, too much contact
		case "hardmode": {_weights = [1,1,0.7,0.3];}; // The old default setting
		default {_weights = [1,1,0.3,0];};
	};
};

// If the amount is a negative number, treat it as a ratio.
if (_amount < 0) then {
	_multiplier = _amount * -1; // Make the multiplier a positive number
	_amount = DEFAULTZOMBIEAMOUNT * _multiplier;
};

_triggerPos = getPos _trigger;
_activeDist = (sqrt ((_triggerX^2)+(_triggerY^2))) + 150; // Accounts for the corners of the square being further away from the trigger
_minDist = 125; // Prevents spawns within x distance meters of a player

private ["_groups", "_units"];
_groups = [];
_units = [];

// Wait until there are players in range

waitUntil {
	sleep 15; // Only check this every 15 seconds
	({(_triggerPos distance _x) < _activeDist} count allPlayers > 0)
};

while {_amount > 0} do {
	// Start spawning
	private ["_pos","_type"];

	// Keep attempting to find a location until we get a good one
	_relocate = true;
	while {_relocate} do {		
		//_pos = [(_triggerPos select 0) + ((random (_triggerX * 2)) - _triggerX), (_triggerPos select 1) + ((random (_triggerY * 2)) - _triggerY), 0];
		_pos = _trigger call BIS_fnc_randomPosTrigger;
		// Check to make sure that our position is inside the trigger, on land, and away from any players
		if ((_pos inArea _trigger) and (!surfaceIsWater _pos) and (({(_pos distance [getPos (vehicle _x) select 0, getPos (vehicle _x) select 1, 0]) < _minDist} count allPlayers) == 0)) then {_relocate = false};
	};
	// Determine the type to spawn
	_speed = [0,1,2,3] selectRandomWeighted _weights;
	_type = _array select _speed;
	
	// Determine how many to spawn
	_spawnCount = (floor (random (_amount min (DEFAULTMAXGROUPSIZE - _speed)))) + 1;
	
	// If the group limit has been reached, pause group creation
	// Leave some room in case zeus needs to spawn something though
	waitUntil {((east countSide allGroups) + (independent countSide allGroups)) < 550};
	_side = if ((east countSide allGroups) < 275) then {east} else {independent};
	_group = createGroup [_side,true];
	_groups pushBackUnique _group;
	
	for "_i" from 1 to _spawnCount do {
		_z = _group createUnit [(selectRandom _type), _pos, [], 5, "NONE"];
		[_z] joinSilent _group;
		[_z,"AmovPercMstpSnonWnonDnon_SaluteOut"] remoteExec ["switchMove",0];
		_units pushBackUnique _z;
	};
	
	// Move the zombies over to the HC if present
	if (XPT_headless_connected) then {
		_group setGroupOwner XPT_headless_clientID;
	};
	
	// TEMP - Create a marker as a debug measure to make sure it works
	/*
	_marker = createMarkerLocal [format ["marker%1", random 1000000], _pos];
	_marker setMarkerTypeLocal "hd_dot";
	_marker setMarkerSizeLocal [0.5,0.5];
	
	switch _spawnCount do {
		case 1: {_marker setMarkerColorLocal "ColorBlue"; _marker setMarkerTextLocal "1";};
		case 2: {_marker setMarkerColorLocal "ColorGreen"; _marker setMarkerTextLocal "2";};
		case 3: {_marker setMarkerColorLocal "ColorYellow"; _marker setMarkerTextLocal "3";};
		case 4: {_marker setMarkerColorLocal "ColorOrange"; _marker setMarkerTextLocal "4";};
		case 5: {_marker setMarkerColorLocal "ColorRed"; _marker setMarkerTextLocal "5";};
	};
	*/
	_amount = _amount - _spawnCount;
	
};

// Add all zombies spawned to all curators
{
	_x addCuratorEditableObjects [_units, true];
} forEach allCurators;

// Wait until all players have left the area
waitUntil {
	sleep 60; // Only check this every 60 seconds
	({(_triggerPos distance _x) < (_activeDist) * 2} count allPlayers <= 0)
};

// Start zombie cleanup
{
	if (alive _x) then {
		_x setDamage 1;
		_amount = _amount + 1;
	};
	hideBody _x;
} forEach _units;

// If we have any available units left, restart the script.
if (_amount > 0) then {
	[_trigger, _amount] spawn SXP_fnc_zSpawnArea;
};