// Function for updating mission tasks when objectives are completed.
if (!isServer) exitWith {};
switch (toLower (_this select 0)) do {
	case "celltower_generator": {
		// Re-enable the radios
		SXP_radiosEnabled = true;
		publicVariable "SXP_radiosEnabled";
		[] remoteExec ["SXP_fnc_radioToggle", 0];
		
		playSound3D [soundPath + "media\sounds\generator_start.ogg", celltower_generator];
		celltower_generator setVariable ["started", true, true];
		[celltower_generator, [20,100], "town_easy", 0, true] spawn SXP_fnc_zSpawnHordeRadius;
		["celltower", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[
			[true, "zeus_unit"],
			["generalstore", "survive"],
			"task_generalstore",
			generalstore_location,
			"ASSIGNED",
			100,
			true,
			"rearm",
			true
		] call BIS_fnc_taskCreate;
		[] call MLY_fnc_planeCrash;
	};
	case "generalstore_entry": {
		["generalstore", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		[generalstore_location, [20,200], "town_easy", 0, true] spawn SXP_fnc_zSpawnHordeRadius;
	};
	case "generalstore_tv": {
		[
			[true, "zeus_unit"],
			["policestation", "survive"],
			"task_policestation",
			policestation_location,
			"ASSIGNED",
			100,
			true,
			"move",
			true
		] call BIS_fnc_taskCreate;
	};
	case "policestation": {
		["policestation", "CANCELED", true] call BIS_fnc_taskSetState;
		["policestation", [
			"There was a helicopter at the police station, but it was too low on fuel. No one who is sane would try to fly a helicopter with so little fuel on board.",
			"Search the police station",
			""
		]] call BIS_fnc_taskSetDescription;
		[police_helipad, [25,200], "town_easy", 2, true] spawn SXP_fnc_zSpawnHordeRadius;
	};
	case "policestation_destroyed": {
		["policestation", "CANCELED", true] call BIS_fnc_taskSetState;
		["policestation", [
			"There was a helicopter at the police station, but it heavily damaged. There's no way to fly a helicopter in that condition.",
			"Search the police station",
			""
		]] call BIS_fnc_taskSetDescription;
		[police_helipad, [25,200], "town_easy", 2, true] spawn SXP_fnc_zSpawnHordeRadius;
	};
	case "policestation_radio_start": {
		policeRadio setVariable ["listenedTo", false];
		[] spawn {
			while {!(policeRadio getVariable "listenedTo")} do 
			{
				_sound = soundPath + "media\sounds\militaryDistressCall.ogg"; 
				playSound3D [_sound, policeRadio, false, getPosASL policeRadio, 3, 1, 40];
				sleep 165;
			};
		};
	};
	
	case "policestation_radio_end": {
		[] spawn {
			sleep 20;
			["create_military_base"] call SXP_fnc_updateTask;
			policeRadio setVariable ["listenedTo", true];
		}
	};
	
	case "create_military_base": {
		[
			[true, "zeus_unit"],
			["militarybase", "survive"],
			"task_militarybase",
			military_base_location,
			"ASSIGNED",
			100,
			true,
			"move",
			true
		] call BIS_fnc_taskCreate;
	};
	
	case "military_phone_ring": {
		militaryPhone setVariable ["pickingUp", false, true];
		militaryPhone setVariable ["pickedUp", false, true];
		militaryPhone setObjectTextureGlobal [0, "media\inspectables\phoneCalling.paa"];
		[] spawn {
			private _sound = soundPath + "media\sounds\cellPhoneRing.ogg";
			while {!(militaryPhone getVariable ["pickedUp", false])} do 
			{
				if (!(militaryPhone getVariable ["pickingUp", false])) then {
					playSound3D [_sound, militaryPhone, false, getPosASL militaryPhone, 3, 1, 40];
				};
				sleep 3;
			};
		};
	};
	
	case "military_phone_pickup": {
		["militarybase", "SUCCEEDED", true] call BIS_fnc_taskSetState;
		militaryPhone setVariable ["pickedUp", true, true];
		militaryPhone setObjectTextureGlobal [0, "media\inspectables\phonePickedUp.paa"];
		_sound = soundPath + "media\sounds\scientistPhoneCall.ogg"; 
		playSound3D [_sound, militaryPhone, false, getPosASL militaryPhone, 6, 1, 40];
		[] spawn {
			uiSleep 25;
			[
				[true, "zeus_unit"],
				["klen", "survive"],
				"task_klen",
				klen_location,
				"ASSIGNED",
				100,
				true,
				"danger",
				true
			] call BIS_fnc_taskCreate;
			militaryPhone setObjectTextureGlobal [0, "media\inspectables\phoneOver.paa"];
		};
	};
	
	case "klen_complete": {
		["klen", "SUCCEEDED", true] call BIS_fnc_taskSetState;
	};
};