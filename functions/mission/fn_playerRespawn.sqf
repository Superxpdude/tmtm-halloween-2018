/*
	SXP_fnc_playerRespawn
	Author: Superxpdude
	Handles moving players around when they respawn
	
	Spawned from onPlayerRespawn
	
	Returns: Nothing
*/

_this params ["_newUnit", "_oldUnit", "_respawn", "_respawnDelay"];
private ["_pos"];

private _findSpawnPos = {
	private ["_spawnPos"];
	if (({(alive _x) AND (_x != player)} count (units (group player))) > 0) then {
		// If so, pick a squad member, and spawn on them.
		//_spawnPos = getPos (selectRandom ({alive _x} count (units (group player))));
		_spawnPos = getPos (selectRandom ((units (group player)) select {(alive _x) AND (_x != player)}));
	} else {
		// If no other group members are alive, find another group to spawn on.
		//_spawnPos = getPos (leader (selectRandom ({(isPlayer (leader _x)) AND (side (leader _x) == west)} count allGroups)));
		_spawnPos = getPos (leader (selectRandom (allGroups select {(isPlayer (leader _x)) AND (side (leader _x) == west) AND ((leader _x) != player)})));
	};
	if (isNil "_spawnPos") then {
		_spawnPos = getPos _oldUnit;
	};
	_spawnPos
};

// Check if the player is a respawn, or a JIP.
if ((!isNull _oldUnit) OR didJip) then {
	// Respawn mid-mission
	// Check if the player is a group leader
	if ((leader (group player)) == player) then {
		// If a group leader, check if any other squad members are alive
		_pos = [] call _findSpawnPos;
	} else {
		// If not a group leader, check if the group leader is alive
		if (alive (leader (group player))) then {
			_pos = getPos (leader (group player));
		} else {
			_pos = [] call _findSpawnPos;
		};
	};
	
	// Find the closest spawn point
	private _dist = SXP_respawnLocations apply {_pos distance2D _x};
	private _i = _dist find (selectMin _dist);
	_teleportPos = getPosATL (SXP_respawnLocations select _i);
	
	// Move the player
	player setPosATL _teleportPos;
	
	sleep 1;
	cutText ["", "BLACK IN", 3];
	titleText [format ["You wake up somewhere near %1", text (nearestLocations [player, ["nameVillage", "nameCity", "nameCityCapital"], worldSize, player] select 0)], "PLAIN", 0.5];
} else {
	// Game start spawning
	if ((leader (group player)) == player) then {
		[group _newUnit] remoteExec ["SXP_fnc_groupSpawnInit", 2];
	};
	waitUntil {!isNil {(group player) getVariable "sxp_spawnpoint"}};
	// Find a safe position to spawn
	_pos = [getPos ((group player) getVariable "sxp_spawnpoint"),0,10,1,0,1,0,[],getPos ((group player) getVariable "sxp_spawnpoint")] call BIS_fnc_findSafePos;
	player setPos _pos;
	
	sleep 1;
	cutText ["", "BLACK IN", 3];
};

// If no old unit exists, play the title card. (Usually meaning missio start)
if (isNull _oldUnit) then {
	sleep 5;
	[parseText format ["<t align='right' size='1.6'><t font='PuristaBold' size='1.8'>%1<br/></t>%2<br/>%3:%4:00</t>",
		toUpper "A Walk in the Woods", 
		"by Superxpdude and O'Mally", 
		date select 3,
		date select 4],
		true,
		nil,
		10
	] call BIS_fnc_textTiles;
};