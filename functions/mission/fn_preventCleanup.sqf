// Function to stop a corpse from being cleaned up by the remains collector

if (!isServer) exitWith {};

if (!(_this isEqualType [])) then {
	_this = [_this];
};

{
	_x spawn {
		while {true} do {
			waitUntil {isInRemainsCollector _this};
			removeFromRemainsCollector [_this];
		};
	};
} forEach _this;