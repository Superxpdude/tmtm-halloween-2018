// SXP_fnc_setupVehcile
// Handles setting up a specific vehicle on mission start.
// Only runs on the server

if (!isServer) exitWith {};

// Define parameters
params [
	["_veh", nil, [objNull]],
	["_type", nil, [""]]
];

// Check to make sure both variables are defined
if ((isNil "_veh") or (isNil "_type")) exitWith {};

switch _type do {
	case "crashed_humvee": {
		_veh engineOn true;
		_veh setPilotLight true;
		_veh setFuel 0.04;
		[_veh, "crashed_humvee"] call XPT_fnc_loadItemCargo;
	};
	case "store_truck": {
		_veh setFuel 0.06;
		[_veh, "store_truck"] call XPT_fnc_loadItemCargo;
	};
	case "farm_truck": {
		_veh setFuel ((random 4)/100) + 0.02;
		_veh setDamage ((random 10)/100);
		[_veh, "farm_truck"] call XPT_fnc_loadItemCargo;
	};
	case "civ_truck": {
		_veh setFuel ((random 6)/200) + 0;
		_veh setDamage ((random 50)/100);
		[_veh, "civ_truck"] call XPT_fnc_loadItemCargo;
	};
	case "factory_truck": {
		_veh setFuel ((random 1)/100) + 0.02;
		_veh setDamage ((random 50)/100);
		[_veh, "civ_truck"] call XPT_fnc_loadItemCargo;
	};
	case "start_truck": {
		_veh setFuel 0;
		_veh setDamage ((random 30)/100) + 0.5;
		[_veh, "start_truck"] call XPT_fnc_loadItemCargo;
	};
	case "start_truck_crash": {
		_veh setPilotLight true;
		_veh setFuel 0;
		_veh setDamage ((random 15)/100) + 0.75;
		[_veh, "start_truck"] call XPT_fnc_loadItemCargo;
	};
	case "police_truck": {
		_veh setDamage ((random 40)/100);
		_veh setFuel ((random 2)/100) + 0.01;
		[_veh, "police_truckbox"] call XPT_fnc_loadItemCargo;
	};
	case "police_heli": {
		_veh setDamage ((random 10)/100);
		_veh setFuel 0.003;
		[_veh, "police_heli"] call XPT_fnc_loadItemCargo;
	};
	case "research_truck": {
		_veh setDamage ((random 30)/100) + 0.6;
		_veh setFuel 0;
		[_veh, "empty"] call XPT_fnc_loadItemCargo;
	};
	case "military_vehicle": {
		_veh setDamage ((random 40)/100) + 0.4;
		_veh setFuel 0;
		_veh setVehicleAmmoDef 0;
		[_veh, "milblue_vehicle"] call XPT_fnc_loadItemCargo;
	};
	case "idap": {
		_veh setDamage ((random 30)/100) + 0.3;
		_veh setFuel 0;
		[_veh, "empty"] call XPT_fnc_loadItemCargo;
	};
};