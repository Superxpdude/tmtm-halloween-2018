// SXP_fnc_setupUnit
// Handles setting up a specific unit on mission start.
// Only runs on the server

if (!isServer) exitWith {};

// Define parameters
params [
	["_unit", nil, [objNull]],
	["_type", nil, [""]]
];

// Check to make sure both variables are defined
if ((isNil "_unit") or (isNil "_type")) exitWith {};

switch _type do {
	case "helipilot": {
		_unit setDamage 1;
		//[_unit] call SXP_fnc_preventCleanup;
	};
	case "humvee_soldier": {
		_unit setDamage 1;
		_unit setUnitLoadout [[],[],[],["rhs_uniform_cu_ucp",[]],["rhsusf_iotv_ucp_Rifleman",[]],[],"rhsusf_ach_helmet_ucp","rhs_ess_black",[],["","","","ItemCompass","ItemWatch",""]];
		//[_unit] call SXP_fnc_preventCleanup;
	};
	case "crashed_pilot": {
		_unit setDamage 1;
		//[_unit] call SXP_fnc_preventCleanup;
	};
	case "scientist": {
		_unit setDamage 1;
		_unit setUnitLoadout [[],[],[],["U_C_Scientist",[]],[],[],"","",[],["","","","ItemCompass","ItemWatch",""]];
		//[_unit] call SXP_fnc_preventCleanup;
	};
	case "policeman": {
		_unit setDamage 1;
		_unit setUnitLoadout [[],[],["rhs_weap_makarov_pm","","","",["rhs_mag_9x18_8_57N181S",7],[],""],["CUP_U_C_Policeman_01",[["FirstAidKit",1]]],["CUP_V_C_Police_Holster",[]],[],"CUP_H_C_Policecap_01","G_Squares_Tinted",[],["","","","","ItemWatch",""]];
		//[_unit] call SXP_fnc_preventCleanup;
	};
};