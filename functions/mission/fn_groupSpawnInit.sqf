/*
	SXP_fnc_groupSpawnInit
	Author: Superxpdude
	Handles assigning original spawn points to player groups
	
	Remoteexec'd from the group leader of the group
	
	Returns: Nothing
*/
if (!isServer) exitWith {};

params [
	["_group", grpNull, [grpNull]]
];

if (isNull _group) exitWith {};

// If we run out, reset the list
if (count SXP_spawnpoints <= 0) then {
	SXP_spawnpoints = SXP_spawnpoints_base;
};

private _spawn = selectRandom SXP_spawnpoints;
SXP_spawnpoints deleteAt (SXP_spawnpoints find _spawn);
_group setVariable ["sxp_spawnpoint", _spawn, true];