_this params ["_obj", "_texture", "_text", "_soundFilePath", "_soundLength"];

[_obj, [_texture, 0, -1], _text, ""] call BIS_fnc_initInspectable;

if ((!isNil "_soundFilePath") AND (isServer)) then 
{
	[_obj, _soundFilePath, _soundLength] spawn 
	{
		_this params ["_obj", "_soundFilePath", "_soundLength"];
		while {true} do 
		{
			_sound = soundPath + _soundFilePath; 
			playSound3D [_sound, _obj, false, getPosASL _obj, 2, 1, 30];
			sleep _soundLength;
		};
	};
};