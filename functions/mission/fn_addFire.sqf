_this params ["_obj", "_source","_source1","_source2", "_freq", "_weight"];

_particleParams = [
	["\A3\data_f\ParticleEffects\Universal\Universal_02", /*String*/8, /*Number*/0, /*Number*/40, /*Number*/1 /*Optional - Number. Default: 1*/],
	"", /*String*/
	"Billboard", /*String - Enum: Billboard, SpaceObject*/
	1, /*Number*/
	10, /*Number*/
	[0,0,0], /*3D Array of numbers as relative position to particleSource or (if object at index 18 is set) object. 
			Or (if object at index 18 is set) String as memoryPoint of object.*/
	[0.5,0.75,0.5], /*3D Array of numbers.*/
	1, /*Number*/
	_weight, /*Number*/
	0.04, /*Number*/
	0.05, /*Number*/
	[4, 16], /*Array of Number*/
	[[0.35,0.35,0.35,0.6],[0.35,0.35,0.35,0.75],[0.35,0.35,0.35,0.45],[0.42,0.42,0.42,0.28],[0.42,0.42,0.42,0.16],[0.42,0.42,0.42,0.09],[0.42,0.42,0.42,0.06],[0.5,0.5,0.5,0.02],[0.5,0.5,0.5,0]],
	[1,0.55,0.35], /*Array of Number*/
	0.3, /*Number*/
	0.2, /*Number*/
	"", /*String*/
	"", /*String*/
	_obj /*Object*/
];

//_source1 setParticleCircle [0, [0, 0, 0]];
_source1 setParticleRandom [0, [0.5, 0.5, 0], [0.2, 0.2, 0], 0, 0.25, [0, 0, 0, 0.1], 0, 0];
_source1 setParticleParams [["\Ca\Data\ParticleEffects\FireAndSmokeAnim\SmokeAnim.p3d", 8, 1, 6], "", "Billboard", 1, 15, [0, 0, 0], [0, 0, 4.5], 0, 8, 7.9, 0.5, [4, 12, 20], [[0.1, 0.1, 0.1, 0.8], [0.25, 0.25, 0.25, 0.5], [0.5, 0.5, 0.5, 0]], [0.125], 1, 0, "", "", _obj];
_source1 setDropInterval _freq;

//_source2 setParticleCircle [0, [0, 0, 0]];
/*_source2 setParticleRandom [0, [0.25, 0.25, 0], [0.2, 0.2, 0], 0, 0.25, [0, 0, 0, 0.1], 0, 0];
_source2 setParticleParams [["\Ca\Data\ParticleEffects\FireAndSmokeAnim\SmokeAnim.p3d", 8, 1, 8], "", "Billboard", 1, 15, [0, 0, 0], [0, 0, 2.5], 0, 7, 7.9, 0.066, [2, 6, 12], [[0.1, 0.1, 0.1, 1], [0.25, 0.25, 0.25, 0.5], [0.5, 0.5, 0.5, 0]], [0.125], 1, 0, "", "", _source2];
_source2 setDropInterval _freq * 0.6;*/


_source setParticleClass "AirObjectDestructionFire";
/*_source1 setParticleClass "AirObjectDestructionSmoke";
_source1 setParticleParams _particleParams;
_source1 setDropInterval _freq;
_source2 setParticleClass "AirObjectDestructionSmoke1";
_source2 setParticleParams _particleParams;
_source2 setDropInterval _freq * 0.6;*/


