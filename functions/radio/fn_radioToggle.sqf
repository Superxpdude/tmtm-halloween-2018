// Function to toggle the status of player radios.
// To be executed on the local machine. Takes no parameters
if (!hasInterface) exitWith {};
if (isNil "SXP_radiosEnabled") exitWith {};

if (SXP_radiosEnabled) then {
	player setVariable ["tf_receivingDistanceMultiplicator", 1];
	player setVariable ["tf_sendingDistanceMultiplicator", 1];
	player linkItem "ItemMap";
} else {
	player setVariable ["tf_receivingDistanceMultiplicator", 0];
	player setVariable ["tf_sendingDistanceMultiplicator", 0];
	player unlinkItem "ItemMap";
};