// First stage boss function

// Start the revive loop
[] spawn SXP_fnc_boss_reviveLoop;
[] spawn SXP_fnc_boss_captiveLoop;

// Start teleporting the player
[] spawn {
	//[0, "BLACK", 3] spawn BIS_fnc_fadeEffect;
	cutText ["You're walking in the woods", "BLACK", 2];
	sleep 2.2;
	// If the player is dead, respawn them
	if (!alive player) then {
		setPlayerRespawnTime 0;
	};
	// If the player is unconscious, heal them
	if (player getVariable ["ACE_isUnconscious", false]) then {
		[player, player] call ace_medical_fnc_treatmentAdvanced_fullHealLocal;
	};
	// Find a position within the boss fight area
	private _pos = boss_area_trigger call BIS_fnc_randomPosTrigger;
	player setPos _pos;
	sleep 0.8;
	cutText ["You're walking in the woods", "BLACK IN", 2];
	sleep 2.1;
	cutText ["", "PLAIN", 1];
};

// Start the music
0 fadeMusic 1;
0 fadeSound 1;
playMusic "shia_laboeuf";

waitUntil {getMusicPlayedTime > 4.46};
// Your phone is dead.
["phonedead"] call BIS_fnc_showNotification;
[] call SXP_fnc_radioToggle;

waitUntil {getMusicPlayedTime > 68.9};

// YOUR LEG IS CAUGHT IN A BEAR TRAP
if (selectRandom [true,false]) then {
	[player, 0.5, "leg_l", "stab"] call ACE_medical_fnc_addDamageToUnit;
} else {
	[player, 0.5, "leg_r", "stab"] call ACE_medical_fnc_addDamageToUnit;
};
["beartrap"] call BIS_fnc_showNotification;

// Heal the player once Shia is dead
waitUntil {getMusicPlayedTime > 100};
[player, player] call ace_medical_fnc_treatmentAdvanced_fullHealLocal;