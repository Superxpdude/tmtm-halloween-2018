// Second stage boss function

// Make sure that the player is alive and well
[player, player] call ace_medical_fnc_treatmentAdvanced_fullHealLocal;

// Kill the existing end mission overlays
0 fadeMusic 1;
0 fadeSound 1;

private _layerNoise = "BIS_fnc_endMission_noise" call bis_fnc_rscLayer;
private _layerInterlacing = "BIS_fnc_endMission_interlacing" call bis_fnc_rscLayer;
private _layerStatic = "BIS_fnc_endMission_static" call bis_fnc_rscLayer;
private _layerEnd = "BIS_fnc_endMission_end" call bis_fnc_rscLayer;
{_x cuttext ["","plain"]} foreach [_layerNoise,_layerInterlacing,_layerStatic,_layerEnd];

showhud true;

["shia_surprise"] call BIS_fnc_showNotification; 
playMusic "shia_surprise";

waitUntil {getMusicPlayedTime > 11};

// LEGENDARY FIGHT
player removeWeapon (primaryWeapon player);
player addWeapon "rhs_weap_m249_pip_S";
{
	player addPrimaryWeaponItem _x
} forEach ["rhsusf_200rnd_556x45_mixed_box","rhsusf_acc_compm4","acc_flashlight"];
//if ((currentWeapon player)
player selectWeapon "rhs_weap_m249_pip_S";
//player addeventhandler ["fired", {(_this select 0) setvehicleammo 1}];
player addeventhandler ["fired", {(_this select 0) setAmmo [currentWeapon (_this select 0), 100000];}];
["legendary_fight"] call BIS_fnc_showNotification;

// Heal the player once Shia is dead for real
waitUntil {getMusicPlayedTime > 30};
[player, player] call ace_medical_fnc_treatmentAdvanced_fullHealLocal;