// Loop that runs during the boss fight. Ensures that players don't actually die during the boss fight
// Only to be run on player machines

if (!hasInterface) exitWith {};

while {true} do {
	if (player getVariable ["ACE_isUnconscious", false]) then {
		[player, player] call ace_medical_fnc_treatmentAdvanced_fullHealLocal;
		systemChat "You're not allowed to die yet!";
	};
	sleep 15;
};