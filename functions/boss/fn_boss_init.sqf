// Boss fight init function
// Handles starting the boss fight from the server
// Designed to be remoteExec'd from a player using a holdaction

// Only to be run on the server
if (!isServer) exitWith {};

#define _TRIGGER_ boss_init_trigger
#define _PLAYERCOUNT_ 8
#define _ZOMBIECOUNT_ 8

// Define variables
private _type = ["RyanZombieC_man_1slow", "RyanZombieC_man_polo_1_Fslow", "RyanZombieC_man_polo_2_Fslow", "RyanZombieC_man_polo_4_Fslow", "RyanZombieC_man_polo_5_Fslow", "RyanZombieC_man_polo_6_Fslow", "RyanZombieC_man_p_fugitive_Fslow", "RyanZombieC_man_w_worker_Fslow", "RyanZombieC_scientist_Fslow", "RyanZombieC_man_hunter_1_Fslow", "RyanZombieC_man_pilot_Fslow", "RyanZombieC_journalist_Fslow", "RyanZombieC_Orestesslow", "RyanZombieC_Nikosslow", "RyanZombie15slow", "RyanZombie16slow", "RyanZombie17slow", "RyanZombie18slow", "RyanZombie19slow", "RyanZombie20slow", "RyanZombie21slow", "RyanZombie22slow", "RyanZombie23slow", "RyanZombie24slow", "RyanZombie25slow", "RyanZombie26slow", "RyanZombie27slow", "RyanZombie28slow", "RyanZombie29slow", "RyanZombie30slow", "RyanZombie31slow", "RyanZombie32slow"];
if (isNil "SXP_boss_init_zList") then {
	SXP_boss_init_zList = [];
};


// Check if there are above the required number of players within the circle
if ((({(isPlayer _x) AND (alive _x)} count (list _TRIGGER_)) >= (_PLAYERCOUNT_ min ({alive _x} count (allPlayers - entities "HeadlessClient_F")))) AND (["klen"] call BIS_fnc_taskExists)) then {
	// Make some effects happen here
	// Start the boss fight script
	boss_stone setVariable ["activated", true, true];
	["klen", "SUCCEEDED", true] call BIS_fnc_taskSetState;
	[] spawn {
		// Code here taken from BIS_fnc_moduleLightning
		private _group = createGroup west;
		for "_i" from 1 to 8 do {
			_group createUnit ["ModuleLightning_F", getposAtl boss_stone, [], 0, "CAN_COLLIDE"];
			uiSleep ((random 1) + 1)/2;
		};
		uiSleep 2;
		[] spawn SXP_fnc_boss_server;
	};
} else {
	// If we're missing people, play a sound and spawn some zombies
	// Play a sound
	[_type] spawn {
		// Make an earthquake effect
		[2] remoteExec ["BIS_fnc_earthquake", 0];
		uiSleep 5;
		// Create the group
		private _group = createGroup [east,true];
		// Spawn 8 zombies within the circle
		for "_i" from 1 to _ZOMBIECOUNT_ do {
			private _pos = _TRIGGER_ call BIS_fnc_randomPosTrigger;
			private _z = _group createUnit [(selectRandom (_this select 0)), _pos, [], 5, "NONE"];
			[_z] joinSilent _group;
			_z switchMove "AmovPercMstpSnonWnonDnon_SaluteOut";
			SXP_boss_init_zList pushBackUnique _z;
			// Add all zombies spawned to all curators
			{
				_x addCuratorEditableObjects [units _group, true];
			} forEach allCurators;
		};
		
		// Move the zombies over to the HC if present
		[_group] call XPT_fnc_headlessSetGroupOwner;
	};
};