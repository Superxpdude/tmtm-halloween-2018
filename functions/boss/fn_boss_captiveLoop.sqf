// Loop that runs during the boss fight. Marks downed players as captive so the boss switches targets.
// Only to be run on player machines

if (!hasInterface) exitWith {};

while {true} do {
	if (player getVariable ["ACE_isUnconscious", false]) then {
		player setCaptive true;
	} else {
		player setCaptive false;
	};
	sleep 1;
};