// Boss fight server function.
// Ensures that all clients run the boss fight in sync

// Only to be run on the server
if (!isServer) exitWith {};

// Disable radios
SXP_radiosEnabled = false;
publicVariable "SXP_radiosEnabled";

// Mark the boss fight as started
missionNameSpace setVariable ["SXP_bossFight",true,true];

// Kill and remove any zombies spawned at the circle
if (!isNil "sxp_boss_init_zList") then {
	{
		if (alive _x) then {
			_x setDamage 1;
		};
		hideBody _x;
	} forEach sxp_boss_init_zList;
};

// Start the client scripts
[] remoteExec ["SXP_fnc_boss_client_stage1", 0];
[] spawn {
	uiSleep 2.2;
	[[date select 0, date select 1, date select 2, 17, 30],true,false] call BIS_fnc_setDate;
};
uiSleep 10;

// Spawn the boss
private _group = createGroup [east,true];
shia = _group createUnit ["RyanZombieboss20Opfor", getPos boss_area_trigger, [], 5, "NONE"];
[shia,"AmovPercMstpSnonWnonDnon_SaluteOut"] remoteExec ["switchMove",0];
shia allowDamage false;

uiSleep 86;
// Kill shia
shia allowDamage true;
shia setDamage 1;

// Wait until the music ends
uiSleep 15;
//uiSleep 5;

["victory_fake"] remoteExec ["SXP_fnc_endMissionFake", 0];

uiSleep 16; // Wait for the end mission to play on clients

// Spawn the new bosses
private _units = [];
private _group2 = createGroup [east,true];
for "_i" from 1 to 5 do {
	private _group2 = createGroup [east,true];
	_z = _group2 createUnit ["RyanZombieboss20Opfor", boss_area_trigger call BIS_fnc_randomPosTrigger, [], 5, "NONE"];
	[_z,"AmovPercMstpSnonWnonDnon_SaluteOut"] remoteExec ["switchMove",0];
	_z allowDamage false;
	_units pushBackUnique _z;
};

[] remoteExec ["SXP_fnc_boss_client_stage2", 0];

uiSleep 28.5;
// Kill the bosses
{
	_x setFace "RyanZombieNoFace";
	_x remoteExecCall ["RZ_fnc_HeadParticles"];
	[_x, "RyanZombieNoFace"] remoteExecCall ["setFace"];
	
	removeHeadgear _x;
	removeGoggles _x;
	
	_hmd = (hmd _x);
	_x unassignItem _hmd;
	_x removeItem _hmd;
	
	
	_headExplodeSound = selectRandom RZ_HeadExplodeArray;
	playsound3d [_headExplodeSound, _x,false, getPosASL _x, 10, 1, 30];
	_x setDamage 1;
} forEach (_units);

uiSleep 15;
["survive", "SUCCEEDED", true] call BIS_fnc_taskSetState;
uiSleep 1;

["victory_real",true,true,true] remoteExec ["BIS_fnc_endMission", 0];