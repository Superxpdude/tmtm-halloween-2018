// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point
[] execVM "scripts\briefing.sqf";

_player unlinkItem "ItemMap";

if (hasInterface) then {
	[
		celltower_generator,
		"Start generator",
		"media\holdActions\holdAction_power.paa",
		"media\holdActions\holdAction_power.paa",
		"!(_target getVariable ['started', false]) && ((player distance _target) < 5)",
		"!(_target getVariable ['started', false]) && ((player distance _target) < 5)",
		nil,
		nil,
		{["celltower_generator"] remoteExec ["SXP_fnc_updateTask", 2];},
		nil,
		[],
		10,
		1000, // Maximum priority
		false,
		false
	] call BIS_fnc_holdActionAdd;
	
	[
		phoneBlock, 
		"Answer Call", 
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa", 
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa", 
		"(_target distance _this < 3) && !(militaryPhone getVariable ['pickedUp', false]) && ['militaryBase'] call BIS_fnc_taskExists", 
		"(_target distance _caller < 3) && !(militaryPhone getVariable ['pickedUp', false]) && ['militaryBase'] call BIS_fnc_taskExists", 
		{militaryPhone setVariable ["pickingUp", true, true]}, 
		{militaryPhone setVariable ["pickingUp", true, true]}, 
		{["military_phone_pickup"] remoteExec ["SXP_fnc_updateTask", 2];}, 
		{militaryPhone setVariable ["pickingUp", false, true]}, 
		[], // No arguments
		4, // 4 seconds to pick up the phone
		1000, // Maximum priority 
		false, 
		false
	] call BIS_fnc_holdActionAdd;
	
	[
		boss_stone, 
		"Perform Ritual", 
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_forcerespawn_ca.paa", 
		"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_forcerespawn_ca.paa", 
		"(_target distance _this < 5) && !(boss_stone getVariable ['activated', false]) && ['klen'] call BIS_fnc_taskExists", 
		"(_target distance _caller < 5) && !(boss_stone getVariable ['activated', false]) && ['klen'] call BIS_fnc_taskExists", 
		{}, 
		{
			_progressTick = _this select 4;
			if (_progressTick % 2 == 0) exitwith {};
			playsound3d [((getarray (configfile >> "CfgSounds" >> "Orange_Action_Wheel" >> "sound")) param [0,""]) + ".wss",player,false,getposasl player,1,0.9 + 0.2 * _progressTick / 24];
		}, // Taken from BIS_fnc_initInspectable
		{[] remoteExec ["SXP_fnc_boss_init", 2];}, 
		{}, 
		[], // No arguments
		10, // 10 seconds to complete the ritual
		1000, // Maximum priority 
		false, 
		false
	] call BIS_fnc_holdActionAdd;
};

// Inspectable setup for the EBS TV
[
	ebsTV, 
	"media\inspectables\ebs3.jpg", 
	"<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><t align='center'><t size='3'>------EMERGENCY BROADCAST SYSTEM------</t><br /><t size='2'>ALL RESIDENTS EVAC TO GORKA POLICE STATION TO AWAIT FURTHER INSTRUCTION</t></t>", 
	"media\sounds\ebsBeeps.ogg",
	13
] call MLY_fnc_setupInspectable;

// Inspectable setup for the notepad in Polana
[
	notePadCube, 
	"media\inspectables\polanaBarnNote.paa", 
	"<br /><br /><br /><br /><br /><br /><br /><t size='2'>Bozidar,<br /> We've left for the Gorka police station, the broadcast says we can't wait for you, please head there if you read this. Remember, the Malanowski's have some supplies in their barn on the North side of town if you need them. Stay safe.<br />-XOXO, Czeslava</t>"
] call MLY_fnc_setupInspectable;

// Inspectable setup for the research notes at Klen
[
	researchCube, 
	"media\inspectables\researchNotes.paa", 
	"<t size='3'>KLEN MONUMENT RESEARCH<br/>Report #2, Oct 31, 2008</t><br/><br/>Introduction<br/>This is the second in a continuing series of reports relating to the Klen monument that appeared seemingly out of nowhere some months ago in northern Chernarus. The monument itself is made up of 8 statues placed in a circle around some sort of altar, with a large stone head outside the formation looking in. From what we can judge, this seems to be some sort of worshiping site to a specific entity, but who this entity is or how the monument came to be is still uncertain.<br/><br/>“Power of Eight”<br/>This week our research mostly focused on trying to translate various inscriptions on the 8 statues and the stone head. While most of the text is worn away or illegible to us, we have noticed a commonly occurring character that seems to be representative of the number eight. Other characters that often show up next to this symbol are ones that refer to “power” and “summoning”. The significance of there being 8 statues (aside from the main head, who we believe to be the object of worship) cannot be overlooked. It’s almost too much of a coincidence that there’s eight of us here. Dr. Kleznov believes that she has translated the “meat” of how this ritual is supposed to go, and we’ll be attempting to recreate the ritual tonight, half as actual research and half as a nice break on halloween. A shame that Dr. Clark is sick, so we lack an eighth person.<br/><br/>Dr. Jeremy West<br/>Head Researcher<br/>Klen Monument Research Commitee"
] call MLY_fnc_setupInspectable;

// Event handler for the EBS TV at the general store
ebsTV setVariable ["inspectedEH", [missionNamespace, "objectInspected", 
	{
		_obj = _this select 0;
		if (_obj == ebsTV) then 
		{
			["generalstore_tv"] remoteExec ["SXP_fnc_updateTask", 2];
			[missionNamespace, "objectInspected", 0] call BIS_fnc_removeScriptedEventHandler; 
		};
	}] call BIS_fnc_addScriptedEventHandler
];