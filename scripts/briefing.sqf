// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord

//player createDiaryRecord ["Diary", ["Assets", "Example Mission Assets"]];
//player createDiaryRecord ["Diary", ["Mission", "Example Mission Briefing"]];
//player createDiaryRecord ["Diary", ["Situation", "Example Situation Briefing"]];

player createDiaryRecord ["Diary", ["Credits", "This mission was made possible with contributions from the following people:
<br/> - Superxpdude: Executive Producer, Writer, and Director
<br/> - Ghost of O'Mally: Executive Director, Special Effects, Environment Design and Sound Production
<br/> - Lanius Aetherion: Executive Voice Actor
"]];

player createDiaryRecord ["Diary", ["Note from the authors", "This mission makes use of clues in the environment to help you find the way to go. Some items are examinable, others have audio, etc. Investigate things you find to be suspicious or noteworthy to advance the mission and find extra goodies."]];

player createDiaryRecord ["Diary", ["Assets", "Your available assets for this mission are:<br/>
	- Literally nothing
"]];

player createDiaryRecord ["Diary", ["Intel",
"Locals reported hearing scary noises outside, so most of them have locked themselves in their houses. A lot of civilian equipment has been left out in the open. Use it if you need to."
]];

player createDiaryRecord ["Diary", ["Mission",
"Your primary objective today is to not die. We don't really know what's going on, but if you can get to the bottom of this mess, do it.
<br/><br/>Your radio cell phones have marked the location of the nearest cell tower, you should meet up there if possible, and try to restore your phone connectivity."
]];

player createDiaryRecord ["Diary", ["Situation",
"Something bad has happened on Chernarus, and a good portion of the country has descended in to pure chaos. No one really knows what's happening, and you just happen to be stuck right in the middle of it.
<br/><br/>During the chaos, it appears that the cell network has been knocked offline. We couldn't afford to buy real radios, and the ones we bought are just cell phones with a fancy case on them. Turns out this was a bad idea, this cell outage will affect our ability to communicate."
]];
