// Notifications system
// Allows consistent looking mission notifications to be displayed (same appearance as task notifications)
// https://community.bistudio.com/wiki/Notification
// Arguments can be passed from BIS_fnc_showNotification, use the percent symbol to signify a value defined by an argument (similar to the format command)

class example
{
	title = ""; // Tile displayed as text on black background. Filled by arguments.
	iconPicture = ""; // Small icon displayed in left part. Colored by "color", filled by arguments.
	iconText = ""; // Short text displayed over the icon. Colored by "color", filled by arguments.
	description = ""; // Brief description displayed as structured text. Colored by "color", filled by arguments.
	color[] = {1,1,1,1}; // Icon and text color
	duration = 5; // How many seconds will the notification be displayed
	priority = 0; // Priority; higher number = more important; tasks in queue are selected by priority
	difficulty[] = {}; // Required difficulty settings. All listed difficulties has to be enabled
};

class cellOnline
{
	title = "Cell network restored";
	iconPicture = "\A3\ui_f\data\igui\cfg\simpleTasks\types\radio_ca.paa";
	iconText = "";
	description = "Communication and maps online";
	color[] = {1,1,1,1};
	duration = 5;
	priority = 1000;
};

class phonedead
{
	title = "Your phone";
	iconPicture = "\A3\ui_f\data\igui\cfg\simpleTasks\types\radio_ca.paa";
	iconText = "";
	description = "It's dead";
	color[] = {1,0.65,0,1};
	colorIconPicture[] = {1,1,1,1};
	colorIconText[] = {1,0,0,1};
	duration = 5;
	priority = 1000;
};

class beartrap
{
	title = "Your leg! AHH!";
	iconPicture = "\A3\ui_f\data\igui\cfg\simpleTasks\types\danger_ca.paa";
	iconText = "";
	description = "It's caught in a bear trap!";
	color[] = {1,0,0,1};
	duration = 5;
	priority = 1000;
};

class shia_surprise
{
	title = "Wait! He isn't dead!";
	iconPicture = "\A3\ui_f\data\igui\cfg\simpleTasks\types\danger_ca.paa";
	iconText = "";
	description = "SHIA SURPRISE!";
	color[] = {1,1,1,1};
	duration = 5;
	priority = 1000;
};

class legendary_fight
{
	title = "Legendary Fight!";
	iconPicture = "\A3\ui_f\data\igui\cfg\simpleTasks\types\rifle_ca.paa";
	iconText = "";
	description = "With Shia Laboeuf!";
	color[] = {1,1,0,1};
	duration = 5;
	priority = 1000;
};