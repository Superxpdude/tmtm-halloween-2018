// Task Descriptions
// Can be used for moving task descriptions from script files to configs


// Create new classes under here. BIS_fnc_taskCreate will use either the class passed as the description parameter (in string format)
// The following would be loaded by setting the description to "example";
// If the description is blank (""), then the task ID will be used automatically
class example
{
	title = "Example Task Title";					// Title of task. Displayed as the task name
	description = "Example Task Description";		// Description of task. Additional details displayed when the task is selected on the map screen.
	marker = "";									// Task marker. Leave blank
};

class task_survive
{
	title = "Survive";
	description = "You're under strict orders not to die. Find a way to get out of there alive.";
	marker = "";
};

class task_celltower
{
	title = "Restore cell connectivity";
	description = "Your radio-phones have marked the location of the last cell tower they were connected to. If you went there, you might be able to restore cell connectivity, including communications and maps.";
	marker = "";
};

class task_generalstore
{
	title = "Acquire weapons";
	description = "Your maps indicate that there's a hunting store in Polana, you may be able to find some better weapons there.";
	marker = "";
};

class task_policestation
{
	title = "Search the police station";
	description = "There was a mandatory evacuation order on the TV, it said to get to the police station in Gorka. There may be a means of escape there.";
	marker = "";
};

class task_militarybase
{
	title = "Search the evac site";
	description = "We heard reports of an evac site being under attack. They sound like they need help.";
	marker = "";
};

class task_klen
{
	title = "Go to Klen";
	description = "The mysterious phone call said something about a ritual up on Klen. We have no idea what it does, but it sounded really important.";
	marker = "";
};