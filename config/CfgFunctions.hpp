// Functions library
// Defines custom functions for the mission. Anything that needs to be called more than once should be a function
// https://community.bistudio.com/wiki/Functions_Library_(Arma_3)

// If you need to add additional functions, create a new section below using your tag of choice (Ex: SXP = Superxpdude)
// See the functions library wiki page for additional details

/*
class EX // Function TAG, used for the first part of the function name
{
	class example // Function category, defines the folder that the file is located in
	{
		class example {}; // Function class. Defines the file 
	};
};
*/
class MLY 
{
	class mission 
	{
		class setupInspectable {}; 
		class addFire {};
		class planeCrash {};
	};
};
class SXP
{
	class boss
	{
		class boss_captiveLoop {};
		class boss_client_stage1 {};
		class boss_client_stage2 {};
		class boss_init {};
		class boss_reviveLoop {};
		class boss_server {};
	};
	class mission
	{
		class endMissionFake {};
		class groupSpawnInit {};
		class loadItemCargoServer {};
		class playerRespawn {};
		class preventCleanup {};
		class setupUnit {};
		class setupVehicle {};
		class updateTask {};
	};
	class radio
	{
		class radioToggle {};
	};
	class z
	{
		class zSpawnArea {};
		class zSpawnHordeRadius {};
		class zSpawnTrap {};
	};
};