// Mission endings
// Handles the mission ending screen
// https://community.bistudio.com/wiki/Debriefing

class example
{
	title = "Example Ending"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "The mission maker should change this"; // Subtitle below the title when the closing shot is triggered
	description = "This should be changed before the mission is finished"; // Description visible on the debriefing screen after the closing shot
	//pictureBackground = ""; // Image file used as a background on the debriefing screen
	//picture = ""; // Icon used for the closing shot
	//pictureColor[] = {1,1,1,1}; // Colour of the icon during the closing shot. Leave as default most of the time
};

class victory_fake
{
	title = "Mission Completed";
	subtitle = "You've beaten Shia Laboeuf.";
	description = "";
	//pictureBackground = "";
	//picture = "";
	//pictureColor[] = {1,1,1,1};
};

class victory_real
{
	title = "Mission Completed";
	subtitle = "You're finally safe from Shia Laboeuf.";
	description = "You've managed to save the day. Who would have guessed that actual cannibal Shia Laboeuf would have been behind all of this?";
	pictureBackground = "media\debrief.paa";
	//picture = "";
	//pictureColor[] = {1,1,1,1};
};