// XPTItemCargos.hpp
// Used for defining ammo box and vehicle item cargos
// XPTVehicleLoadouts can pull definitions from here in order to supply vehicles
// Supports sub-class randomization, if a definition has multiple sub-classes, the script will automatically select one of them to apply to the vehicle/box.

/* 
	Randomization support
	
	--- Sub-class randomization --- 
	
	Any itemCargos class can define items through a sub-class instead of in the main class.
	In this case, the script will pick one of the sub-classes at random to apply to the object.
	The names of the sub-classes do not matter.
	
	The following example will select one of "box1" or "box2" when the "MyAmmoBox" class is called.
	The names of the subclasses don't matter, and they can be named whatever you want them to be.
	
	class itemCargos
	{
		class MyAmmoBox
		{
			class box1
			{
				items[] = {};
			};
			class box2
			{
				items[] = {};
			};
		};
	};
	
	
	--- Per-item randomization ---
	
	Any item can have the quantity changed from a fixed value, to a randomized value.
	By creating an array of three numbers instead of a single one, the itemCargo script will randomly select a quantity using the provided numbers.
	
	Example:
	
	class MyAmmoBox
	{
		items[] = {"FirstAidKit", {1,3,5}}; // Spawns a minimum of 1, a maximum of 5, and an average of 3
	};

*/
class itemCargos
{
	class example
	{
		// Array containing sub-arrays of items to add
		// Sub-arrays must include an item classname, and a quantity
		// The following would add 5 first aid kits to the inventory of the object
		items[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		// Array of items that will only be added when ACE basic medical is being used
		itemsBasicMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		// Array of items that will only be added when ACE advanced medical is being used
		itemsAdvMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
	};
	class civ_weaponbox
	{
		class mosinbox
		{
			items[] = {
				{"rhs_weap_m38", {2,2,4}},
				{"rhsgref_5Rnd_762x54_m38", {6,8,12}},
				{"11Rnd_45ACP_Mag", {0,1,5}}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
		class m1box
		{
			items[] = {
				{"rhs_weap_m1garand_sa43", {2,2,3}},
				{"rhsgref_8Rnd_762x63_Tracer_M1T_M1rifle", {6,8,12}},
				{"11Rnd_45ACP_Mag", {0,1,5}}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
		class civshotgunbox
		{
			items[] = {
				{"rhs_weap_Izh18", {2,2,3}},
				{"rhsgref_1Rnd_00Buck", {12,18,25}},
				{"11Rnd_45ACP_Mag", {0,1,5}}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
	};
	class civ_weaponbox_auto
	{
		class akbox
		{
			items[] = {
				{"rhs_weap_akm", {1,1,2}},
				{"rhs_10Rnd_762x39mm_tracer", {2,3,5}},
				{"rhs_acc_2dpzenit", 2},
				{"11Rnd_45ACP_Mag", {0,1,5}},
				{"CUP_B_AlicePack_Khaki", 1}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
	};
	class polana_barnbox
	{
		items[] = {
			{"rhs_weap_akm", 1},
			{"rhs_10Rnd_762x39mm_tracer", 3},
			{"rhs_acc_2dpzenit", 1},
			{"rhs_weap_M590_8RD", 1},
			{"rhsusf_8Rnd_00Buck", 3},
			{"CUP_B_AlicePack_Khaki", 2}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	class civ_supplybox
	{
		class slashbox
		{
			items[] = {
				{"V_BandollierB_blk", {1,2,3}},
				{"V_BandollierB_rgr", {0,2,3}},
				{"11Rnd_45ACP_Mag", {0,0,10}}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {0,6,10}},
				{"ACE_epinephrine", {0,1,2}},
				{"ACE_morphine", {0,1,5}},
			};
			itemsAdvMed[] = {};
		};
		class backpackbox
		{
			items[] = {
				{"CUP_B_AlicePack_Khaki", {1,2,2}},
				{"B_FieldPack_cbr", {0,1,2}},
				{"B_FieldPack_oli", {0,1,2}},
				{"11Rnd_45ACP_Mag", {0,0,10}}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {0,6,10}},
				{"ACE_epinephrine", {0,1,2}},
				{"ACE_morphine", {0,1,5}},
			};
			itemsAdvMed[] = {};
		};
		class supplybox
		{
			items[] = {
				{"V_BandollierB_blk", {1,2,3}},
				{"V_BandollierB_rgr", {0,2,3}},
				{"CUP_B_AlicePack_Khaki", {0,2,2}},
				{"B_FieldPack_cbr", {1,1,2}},
				{"B_FieldPack_oli", {0,1,2}},
				{"11Rnd_45ACP_Mag", {0,0,10}}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {0,10,15}},
				{"ACE_epinephrine", {0,2,3}},
				{"ACE_morphine", {0,5,10}},
			};
			itemsAdvMed[] = {};
		};
	};
	class airplanebox
	{
		items[] = {
			{"rhs_weap_akm", 2},
			{"rhs_10Rnd_762x39mm_tracer", {4,6,8}},
			{"rhs_acc_2dpzenit", 2},
			{"hgun_Pistol_Signal_F", 1},
			{"6Rnd_RedSignal_F", 3},
			{"hlc_acc_dbalpl_fl", 3},
			{"CUP_B_AlicePack_Khaki", {2,2,3}},
			{"plp_bo_w_BottleTequila", 1}
		};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", {5,14,20}},
			{"ACE_epinephrine", {1,4,6}},
			{"ACE_morphine", {4,8,12}},
			{"ACE_bloodIV", {0,1,2}}
		};
		itemsAdvMed[] = {};
	};
	class medical_small
	{
		items[] = {};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", {5,14,20}},
			{"ACE_epinephrine", {1,4,6}},
			{"ACE_morphine", {4,8,12}},
			{"ACE_bloodIV", {0,1,2}}
		};
		itemsAdvMed[] = {};
	};
	class medical_med
	{
		items[] = {};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", {15,22,35}},
			{"ACE_epinephrine", {3,5,8}},
			{"ACE_morphine", {4,8,12}},
			{"ACE_bloodIV", {2,2,4}}
		};
		itemsAdvMed[] = {};
	};
	class medical_large
	{
		items[] = {};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", {25,38,50}},
			{"ACE_epinephrine", {6,8,12}},
			{"ACE_morphine", {10,18,25}},
			{"ACE_bloodIV", {4,5,8}}
		};
		itemsAdvMed[] = {};
	};
	class civ_medicalbox: medical_small {};
	class police_weaponbox
	{
		class mp5box
		{
			items[] = {
				{"hlc_smg_mp5a2", {1,2,3}},
				{"hlc_30Rnd_9x19_B_MP5", {3,4,6}},
				{"hlc_acc_surefiregrip", 3},
				{"ACE_HandFlare_Green", {1,2,4}},
				{"V_TacVest_blk", {1,2,3}},
				{"B_TacticalPack_blk", {0,1,2}}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
		
		// High chances of getting MP5s
		class mp5box2: mp5box {};
		class mp5box3: mp5box {};
		class pistolbox
		{
			items[] = {
				{"hgun_Pistol_heavy_01_F", {1,2,2}},
				{"optic_mrd", {1,2,2}},
				{"hlc_acc_dbalpl_fl", {1,2,2}},
				{"11Rnd_45ACP_Mag", {3,5,8}},
				{"ACE_HandFlare_Green", {1,2,4}},
				{"V_TacVest_blk", {1,2,3}},
				{"B_TacticalPack_blk", {0,1,2}}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
	};
	class police_sniperbox
	{
		items[] = {
			{"hlc_rifle_PSG1A1_RIS", 1},
			{"hlc_20rnd_762x51_T_G3", {2,2,3}},
			{"optic_dms", 1},
			{"bipod_01_f_blk", 1},
			{"ACE_HandFlare_Green", {2,4,6}},
			{"V_TacVest_blk", 1},
			{"B_TacticalPack_blk", 1}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	class police_shotgunbox
	{
		items[] = {
			{"rhs_weap_M590_8RD", 1},
			{"rhsusf_8Rnd_00Buck", {2,3,4}},
			{"ACE_HandFlare_Green", {2,4,6}},
			{"V_TacVest_blk", 1},
			{"B_TacticalPack_blk", 1}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	class police_truckbox
	{
		items[] = {
			{"11Rnd_45ACP_Mag", {2,3,4}},
			{"rhsusf_8Rnd_00Buck", {0,1,1}},
			{"hlc_30Rnd_9x19_B_MP5", {0,2,3}},
			{"V_TacVest_blk", {0,1,2}},
			{"B_TacticalPack_blk", {0,1,2}}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	class milblue_weaponbox
	{
		class m16box
		{
			items[] = {
				{"rhs_weap_m16a4_carryhandle", {1,2,3}},
				{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", {4,8,12}},
				{"acc_flashlight", {2,2,3}},
				{"V_TacVest_oli", {1,2,2}},
				{"B_TacticalPack_rgr", {0,0,2}}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
		class mk18box
		{
			items[] = {
				{"rhs_weap_mk18", {1,2,3}},
				{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", {4,8,12}},
				{"acc_flashlight", {2,2,3}},
				{"V_TacVest_blk", {1,2,2}},
				{"B_TacticalPack_rgr", {0,0,2}}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
		class hk416box
		{
			items[] = {
				{"rhs_weap_hk416d10_LMT_wd", {1,2,3}},
				{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", {4,8,12}},
				{"acc_flashlight", {2,2,3}},
				{"V_TacVest_blk", {1,2,2}},
				{"B_TacticalPack_rgr", {0,0,2}}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
	};
	class milblue_mgbox
	{
		items[] = {
			{"hlc_lmg_mk48mod1", {1,1,2}},
			{"hlc_100Rnd_762x51_T_M60E4", {2,2,4}},
			{"acc_flashlight", {1,2,2}}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	class milblue_vehicle
	{
		items[] = {
			{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", {2,4,6}},
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	class milred_weaponbox
	{
		class ak74box
		{
			items[] = {
				{"rhs_weap_ak74", {1,2,4}},
				{"rhs_30Rnd_545x39_AK_green", {6,10,16}},
				{"rhs_acc_2dpzenit", {3,3,4}},
				{"V_TacVest_blk", {1,1,2}},
				{"B_TacticalPack_oli", {1,1,2}}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
		class aks74box
		{
			items[] = {
				{"rhs_weap_aks74", {1,2,4}},
				{"rhs_30Rnd_545x39_AK_green", {6,10,16}},
				{"rhs_acc_2dpzenit", {3,3,4}},
				{"V_TacVest_blk", {1,1,2}},
				{"B_TacticalPack_oli", {1,1,2}}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
		class aks74ubox
		{
			items[] = {
				{"rhs_weap_aks74u", {2,2,3}},
				{"rhs_30Rnd_545x39_AK_green", {6,10,12}},
				{"rhs_acc_2dpzenit", {3,3,4}},
				{"V_TacVest_oli", {1,1,2}},
				{"B_TacticalPack_oli", {1,1,2}}
			};
			itemsBasicMed[] = {};
			itemsAdvMed[] = {};
		};
		
	};
	class milred_mgbox
	{
		items[] = {
			{"rhs_weap_pkp", {1,1,2}},
			{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M", {2,2,4}}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	class civ_truck
	{
		class gear
		{
			items[] = {
				{"V_BandollierB_rgr", {1,2,3}},
				{"B_FieldPack_cbr", {0,1,2}},
				{"B_FieldPack_oli", {0,1,2}},
				{"11Rnd_45ACP_Mag", {0,0,4}}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {0,4,8}}
			};
			itemsAdvMed[] = {};
		};
		class mosin
		{
			items[] = {
				{"rhs_weap_m38", 1},
				{"rhsgref_5Rnd_762x54_m38", {3,4,6}},
				{"11Rnd_45ACP_Mag", {0,0,4}}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {0,3,5}}
			};
			itemsAdvMed[] = {};
		};
		class shotgun
		{
			items[] = {
				{"rhs_weap_Izh18", 1},
				{"rhsgref_1Rnd_00Buck", {10,15,20}},
				{"11Rnd_45ACP_Mag", {0,0,4}}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {0,3,5}}
			};
			itemsAdvMed[] = {};
		};
		class medical
		{
			items[] = {
				{"B_FieldPack_cbr", {0,1,2}},
				{"11Rnd_45ACP_Mag", {0,0,4}}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {6,8,10}},
				{"ACE_epinephrine", {0,1,2}},
				{"ACE_morphine", {0,4,6}},
			};
			itemsAdvMed[] = {};
		};
		class misc
		{
			items[] = {
				{"rhs_weap_m38", {0,1,2}},
				{"rhsgref_5Rnd_762x54_m38", {0,6,10}},
				{"V_BandollierB_rgr", {0,1,2}},
				{"B_FieldPack_cbr", {0,1,2}},
				{"B_FieldPack_oli", {0,1,2}},
				{"11Rnd_45ACP_Mag", {0,0,4}}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {0,6,10}},
				{"ACE_epinephrine", {0,1,2}},
				{"ACE_morphine", {0,4,6}},
			};
			itemsAdvMed[] = {};
		};
	};
	class start_truck
	{
		class mosinbox
		{
			items[] = {
				{"rhs_weap_m38", {0,1,2}},
				{"rhsgref_5Rnd_762x54_m38", {0,6,10}},
				{"11Rnd_45ACP_Mag", {0,0,4}}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {0,4,8}}
			};
			itemsAdvMed[] = {};
		};
		class civshotgunbox
		{
			items[] = {
				{"rhs_weap_Izh18", {1,1,2}},
				{"rhsgref_1Rnd_00Buck", {8,12,20}},
				{"11Rnd_45ACP_Mag", {0,0,4}}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {0,4,8}}
			};
			itemsAdvMed[] = {};
		};
	};
	class farm_truck
	{
		class supplies_only
		{
			items[] = {
				{"V_BandollierB_rgr", {1,2,3}},
				{"rhsgref_1Rnd_00Buck", {8,12,20}}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {6,8,12}}
			};
			itemsAdvMed[] = {};
		};
		class shotgun
		{
			items[] = {
				{"rhs_weap_Izh18", 1},
				{"rhsgref_1Rnd_00Buck", {10,15,20}},
				{"V_BandollierB_rgr", 1}
			};
			itemsBasicMed[] = {
				{"ACE_fieldDressing", {2,6,10}}
			};
			itemsAdvMed[] = {};
		};
	};
	class store_truck
	{
		items[] = {
			{"rhs_weap_m38", {3,5,6}},
			{"rhsgref_5Rnd_762x54_m38", {10,15,20}},
			{"rhs_weap_m1garand_sa43", {2,3,4}},
			{"rhsgref_8Rnd_762x63_Tracer_M1T_M1rifle", {6,10,14}},
			{"CUP_B_AlicePack_Khaki", {2,3,4}},
			{"B_FieldPack_cbr", {1,1,3}},
			{"B_FieldPack_oli", {1,1,3}}
		};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", {5,14,20}},
			{"ACE_epinephrine", {1,4,6}},
			{"ACE_morphine", {4,8,12}},
		};
		itemsAdvMed[] = {};
	};
	class police_truck // Unused
	{
		items[] = {
			{"rhs_weap_m4a1_carryhandle", {0,1,2}},
			{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", {4,6,10}},
			{"acc_flashlight", 2}
		};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", {4,6,10}},
			{"ACE_epinephrine", {0,1,3}},
			{"ACE_morphine", {2,3,4}}
		};
		itemsAdvMed[] = {};
	};
	class police_heli // WIP
	{
		items[] = {};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	class crashed_humvee
	{
		items[] = {
			{"rhs_weap_m4a1_carryhandle", 2},
			{"acc_flashlight", 2},
			{"rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red", {2,4,6}},
			{"rhs_mag_m67", 4},
			{"ACE_HandFlare_Green", 4},
			{"V_PlateCarrier1_rgr", 2},
			{"CUP_glaunch_M79", 1},
			{"CUP_1Rnd_StarFlare_Green_M203", 10},
			{"CUP_1Rnd_StarFlare_Red_M203", 10},
			{"B_TacticalPack_rgr", 1}
		};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", {4,6,10}},
			{"ACE_epinephrine", {0,1,3}},
			{"ACE_morphine", {2,3,4}}
		};
		itemsAdvMed[] = {};
	};
	class empty
	{
		items[] = {};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
};