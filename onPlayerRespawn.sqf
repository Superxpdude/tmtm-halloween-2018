// onPlayerRespawn.sqf
// Executes on a player's machine when they respawn
// _this = [<newUnit>, <oldUnit>, <respawn>, <respawnDelay>]
_this params ["_newUnit", "_oldUnit", "_respawn", "_respawnDelay"];

// Call the template onPlayerRespawn function
_this call XPT_fnc_onPlayerRespawn; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point
["Terminate"] call BIS_fnc_EGSpectator;
[] call SXP_fnc_radioToggle;

/*
if (_newUnit getVariable ["squadLeader", false]) then {
	_newUnit addBackpackGlobal "TFAR_rt1523g_black";
};
*/
if (_newUnit isKindOf "B_Soldier_SL_F") then {
	_newUnit addBackpackGlobal "TFAR_rt1523g_black";
};

// If the boss fight has not started, fade out the screen. The boss fight handles respawning on its own.
if (!(missionNamespace getVariable ["SXP_bossFight",false])) then {
	cutText ["", "BLACK FADED", 0];
	_this spawn SXP_fnc_playerRespawn;
} else {
	// Fail-safe in case the boss fight has already started and the player is still dead.
	private _pos = boss_area_trigger call BIS_fnc_randomPosTrigger;
	_newUnit setPos _pos;
};